# skyperole

This small application sets window role properties for Skype windows, running
in an X desktop. I wrote it to get xmonad handle Skype with XMonad.Layout.IM
module.


# Building

## Requirements

* X11 development libraries
* GNU make
* gcc C++ compiler

## Procedure

    git clone https://bitbucket.org/gliktaras/skyperole.git
    cd skyperole
    sed -i -e 's/gliktaras/YOUR_SKYPE_USERNAME/g' skyperole.cpp
    make

Call to sed above can be replaced by manually altering the value of constant
`SKYPE_USERNAME` in file `skyperole.cpp`.


# Usage

To use skyperole, simply start it and leave it running in the background.

## Compatible Skype versions

* 2.2.0.35 Beta.
* 4.0.0.8.

## Window role assignments

| Skype window                 | Window role       |
| Add contact dialog           | skype-add-contact |
| Add to chat dialog           | skype-add-to-chat |
| Chat window                  | skype-chat        |
| Options                      | skype-options     |
| Sign in window               | skype-sign-in     |
| Start conference call dialog | skype-conf-call   |
| The contact list             | skype-main        |
| User profile                 | skype-profile     |


# Skype xmonad integration

As mentioned above, skyperole was written to make Skype for friendly towards
xmonad. To have xmonad position Skype's contact list on the left side of the
workspace, just install xmonad-contrib extensions and modify the layout hook
like so:

    import XMonad.Layout.IM

    -- ...

    myLayouts = withIM 0.2 (Role "skype-main") Grid ||| Full ||| ...

    -- ...

    main = do
        xmonad $ defaultConfig {
            layoutHook = myLayouts
        }

I strongly recommend reading the documentation of the XMonad.Layout.IM module
[here](http://xmonad.org/xmonad-docs/xmonad-contrib/XMonad-Layout-IM.html).


# Reporting Bugs

If you have a bug to report, contact me via Bitbucket or send me an email at
<gliktaras@gmail.com>.


# Licensing

This projects uses BSD 2-Clause license.
